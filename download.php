<?php

require_once __DIR__.'/includes/config.php';
require_once __DIR__.'/includes/functions.php';

if(!isset($_GET['link'])) die('Invalid request');

// Getting link details
$get_link_detail_query = $dbh->prepare('SELECT * FROM product_links INNER JOIN products ON product_links.p_id = products.p_id WHERE product_links.pl_link = ? AND product_links.pl_status=?');
$get_link_detail_query->execute([$_GET['link'],0]);
$link_details = $get_link_detail_query->fetchObject();
 
// Error if not found
if(!$link_details) die('Not found file');

// Reading file
header("Content-type:application/pdf");
header("Content-Disposition:attachment;filename='$link_details->p_name.pdf'");
readfile(__DIR__."/files/$link_details->p_link.pdf");

// Updating downloaded status
$update_downloaded_status = $dbh->prepare('UPDATE `product_links` SET pl_status=? WHERE pl_id=?');
$update_downloaded_status->execute([1,$link_details->pl_id]);

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Download successfully</title>
</head>
<body>
    Successfully downloaded the file <?= $link_details->p_name ?>
</body>
</html>