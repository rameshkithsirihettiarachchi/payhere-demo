<?php
require_once __DIR__.'/includes/config.php';
require_once __DIR__.'/includes/functions.php';

if(!isset($_GET['id'])) die('Invalid request');

$product = get_product($dbh,$_GET['id']);

if(!$product) die('Product not found');

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Buy <?= $product->p_name ?></title>
    <script src="assets/jquery-3.3.1.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.track-input').change(function(e){

                var data = {};
                $('.track-input').each(function(){
                    data[$(this).attr('name')] = $(this).val();
                })
                $('#custom2').val(JSON.stringify(data))
            })
        })
    </script>
</head>
<body>
    <form method="post" action="https://sandbox.payhere.lk/pay/checkout">
        <h2>Purchase <?= $product->p_name ?></h2>
        <input type="hidden" name="merchant_id" value="<?= $merchant ?>">
        <input type="hidden" name="return_url" value="<?= $site_url ?>success.html">
        <input type="hidden" name="cancel_url" value="<?= $site_url ?>error.html">
        <input type="hidden" name="notify_url" value="<?= $site_url ?>notify.php">
        <label for="firstName">
            First Name
            <input type="text" class="track-input" id="firstName" name="first_name"/>
        </label><br>
        <label for="firstName">
            Last Name
            <input type="text" class="track-input" id="lastName" name="last_name"/>
        </label><br>
        <label for="email">
            Your Email
            <input type="email" name="email" id="email"/>
        </label><br>
        <label for="custom">
            Comfirm Email
            <input type="email" name="custom_1" id="custom"/>
        </label><br>
        <span>We are sending the download link to this email</span><br>
        <label for="phone">Phone Number
            <input type="tel" class="track-input" name="phone" id="phone"/>
        </label><br>
        <label for="address">Address
            <input type="text" class="track-input" name="address" id="address"/>
        </label><br>
        
        <label for="city">City
            <input type="text" class="track-input" name="city" id="city"/>
        </label><br>
        
        <label for="country">Country
            <input type="text" class="track-input" name="country" id="country"/>
        </label><br>

        <input type="hidden" name="custom_2" id="custom2" value=""/>

        <input type="hidden" name="order_id" value="<?= $product->p_id ?>" />
        <input type="hidden" name="currency" value="USD" />
        <input type="hidden" name="items" value="<?= $product->p_name ?>" />
        <label for="amount">Amount(USD)
            <input type="number" step="0.01" readonly name="amount" value="<?= $product->p_amount ?>" id="amount"/>
        </label><br>
        <input type="submit" value="Purchase">
    </form>
</body>
</html>
