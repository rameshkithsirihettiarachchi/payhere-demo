<?php
/**
 * Getting product by it's id
 *
 * @param PDO $dbh
 * @param int $id
 * @return object
 */
function get_product($dbh,$id){
    $get_product_query = $dbh->prepare('SELECT * FROM `products` WHERE `p_id`=?');
    $get_product_query->execute([$id]);
    return $get_product_query->fetchObject();
}