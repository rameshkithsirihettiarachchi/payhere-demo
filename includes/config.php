<?php

// Database configurations
$user= 'root';
$pass = '';
$name = 'payhere_demo';
$host = '127.0.0.1';

// Site URL
// NOTE:- add backslash to the end of url
$site_url = 'http://localhost:8000/';

// Payhere
$merchant = '';
$secret = '';

// Email
// First check with disabled smtp. If not working enable it.
$is_smtp = false;

/// SMTP Configurations
$email_host = '';
$email_user = '';
$email_pass = '';
$email_port = 587;
$email_secure = 'tls';

/// From
$from_mail = 'info@payherdemo.lk';
$from_name = 'PayHereDemo';



$dbh = new PDO("mysql:host=$host;dbname=$name", $user, $pass);

