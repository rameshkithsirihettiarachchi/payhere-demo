# Payhere Demo

This is an example to usage of payhere payment gateway for file downloading website.

## Installation

1. Clone or download source codes
2. run `composer install` to install phpmailer
3. Import database schema file(`schema_with_data`) in `./db` directory
4. Edit `./includes/config.php` file as you want
5. Host this project

##Note

This project not working on localhost. Because payhere want a real domain. This project may contain errors and bugs. I haven't tested this.