<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once __DIR__.'/includes/config.php';
require_once __DIR__.'/includes/functions.php';
require_once __DIR__.'/vendor/autoload.php';

$merchant_id         = $_POST['merchant_id'];
$order_id             = $_POST['order_id'];
$payhere_amount     = $_POST['payhere_amount'];
$payhere_currency    = $_POST['payhere_currency'];
$status_code         = $_POST['status_code'];
$md5sig                = $_POST['md5sig'];
$email                = $_POST['custom_1'];

// This variable contain firstname, lastname ....
$data = json_decode($_POST['custom_2']);

$merchant_secret = $secret;

/**
 * @link https://support.payhere.lk/api-&-mobile-sdk/payhere-checkout
 */
$local_md5sig = strtoupper (md5 ( $merchant_id . $order_id . $payhere_amount . $payhere_currency . $status_code . strtoupper(md5($merchant_secret)) ) );

if (($local_md5sig === $md5sig) AND ($status_code == 2) ){

    // Getting product from product id

    $product = get_product($dbh,$order_id);

    // Checking product amount
    if($product->p_amount==$payhere_amount){
        

            // Generating a unique token for the link
            $random = null;
            $token_query = $dbh->prepare('SELECT * FROM `product_links` WHERE `pl_link`=? ');
            do{
                $random = bin2hex(random_bytes(20));

                $token_query->execute([$random]);
                
                $check = $token_query->rowCount();
    
            } while($check); 
            
            // Inserting the link to links table
            $link_insert_query = $dbh->prepare('INSERT INTO `product_links` (`p_id`,`pl_link`,`pl_firstname`,`pl_lastname`,`pl_email`,`pl_phone`,`pl_address`,`pl_city`,`pl_country`,`pl_status`) VALUES (?,?,?,?,?,?,?,?,?,?)');
            $link_insert_query->execute([$order_id,$random,$data->first_name,$data->last_name,$email,$data->phone,$data->address,$data->city,$data->country,0]);

            /**
             * @link https://github.com/PHPMailer/PHPMailer
             */
            $mail = new PHPMailer(true);
            try {
                //Server settings
                $mail->SMTPDebug = 2;                                 // Enable verbose debug output
                if($is_smtp)$mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = $email_host;  // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = $email_user;                 // SMTP username
                $mail->Password = $email_pass;                           // SMTP password
                $mail->SMTPSecure = $email_secure;                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = $email_port;                                    // TCP port to connect to

                //Recipients
                $mail->setFrom($from_mail, $from_name);
                $mail->addAddress($email);               // Name is optional

                //Content
                $mail->isHTML(true);                                  // Set email format to HTML
                $mail->Subject = 'Download '.$product->p_name;

                // Getting email template contents
                $email_template = file_get_contents(__DIR__.'/email.html');

                // Replacing with our contents
                $email_body = str_replace(['{{product_name}}','{{link}}'],[$product->p_name,$site_url.'download.php?link='.$random],$email_template);

                $mail->Body    = $email_body;

                $mail->send();
                echo 'Message has been sent';
            } catch (Exception $e) {
                echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            }

    }

}
