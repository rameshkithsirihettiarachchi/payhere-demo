<?php
require_once __DIR__.'/includes/config.php';
require_once __DIR__.'/includes/functions.php';


// Getting products from product table
$get_products_query = $dbh->prepare('SELECT * FROM `products`');
$get_products_query->execute();
$products = $get_products_query->fetchAll(PDO::FETCH_OBJ);

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pay Here Demo</title>
</head>
<body>
    <h5>Products</h5>
    <hr>
    <?php  foreach($products as $product){?>
        <div class="product-block">
            <h3><?= $product->p_name ?></h3>
            <h5>$<?= $product->p_amount ?></h5>
            <a href="products.php?id=<?= $product->p_id ?>">Buy</a>
            <hr>
        </div>
    <?php } ?>
</body>
</html>
