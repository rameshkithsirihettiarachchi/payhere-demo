-- MySQL Script generated by MySQL Workbench
-- Sat Nov  3 18:33:48 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Table `products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `products` (
  `p_id` INT NOT NULL AUTO_INCREMENT,
  `p_name` VARCHAR(45) NULL,
  `p_grade` INT NULL,
  `p_amount` DECIMAL(8,2) NULL,
  `p_link` VARCHAR(45) NULL,
  PRIMARY KEY (`p_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `product_links`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `product_links` (
  `pl_id` INT NOT NULL AUTO_INCREMENT,
  `p_id` INT NULL,
  `pl_link` VARCHAR(45) NULL,
  `pl_firstname` VARCHAR(45) NULL,
  `pl_lastname` VARCHAR(45) NULL,
  `pl_email` VARCHAR(45) NULL,
  `pl_phone` VARCHAR(45) NULL,
  `pl_address` VARCHAR(45) NULL,
  `pl_city` VARCHAR(45) NULL,
  `pl_country` VARCHAR(45) NULL,
  `pl_status` TINYINT(1) NULL,
  PRIMARY KEY (`pl_id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
