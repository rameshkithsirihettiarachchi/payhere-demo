-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2018 at 04:32 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `payhere_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `p_id` int(11) NOT NULL,
  `p_name` varchar(45) DEFAULT NULL,
  `p_grade` int(11) DEFAULT NULL,
  `p_amount` decimal(8,2) DEFAULT NULL,
  `p_link` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`p_id`, `p_name`, `p_grade`, `p_amount`, `p_link`) VALUES
(1, 'Paper 1', 1, '5.00', 'pdf_1'),
(2, 'Paper 2', 2, '10.00', 'pdf_2');

-- --------------------------------------------------------

--
-- Table structure for table `product_links`
--

CREATE TABLE IF NOT EXISTS `product_links` (
  `pl_id` INT NOT NULL AUTO_INCREMENT,
  `p_id` INT NULL,
  `pl_link` VARCHAR(45) NULL,
  `pl_firstname` VARCHAR(45) NULL,
  `pl_lastname` VARCHAR(45) NULL,
  `pl_email` VARCHAR(45) NULL,
  `pl_phone` VARCHAR(45) NULL,
  `pl_address` VARCHAR(45) NULL,
  `pl_city` VARCHAR(45) NULL,
  `pl_country` VARCHAR(45) NULL,
  `pl_status` TINYINT(1) NULL,
  PRIMARY KEY (`pl_id`))
ENGINE = InnoDB;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `product_links`
--
ALTER TABLE `product_links`
  ADD PRIMARY KEY (`pl_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_links`
--
ALTER TABLE `product_links`
  MODIFY `pl_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
